# how to alert with slack
href: [Link](https://cuongquach.com/cau-hinh-jenkins-pipeline-tich-hop-slack.html) 

custom   
```
// def noti slack
def notifyBuild(String buildStatus = 'CHẠY RỒI ÔNG GIÁO ƠI') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def now = new Date()
  String timedate = now.format("YYYY-MM-DD HH:mm:ss.Ms")

  def msg_details = """${buildStatus}: Form-fe
  Job Name: ${env.JOB_NAME}
  Build: ${env.BUILD_NUMBER}
  Check console output this build at: ${env.BUILD_URL}
  """

  // Override default values based on build status
  if (buildStatus == 'CHẠY RỒI ÔNG GIÁO ƠI') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  }else if (buildStatus == 'CÓ THẰNG NÀO DỪNG KÌA') {
    color = 'BROWN'
    colorCode = '#420505'
  }else if (buildStatus == 'ĐANG TEST NÈ') {
    color = 'BLUE'
    colorCode = '#0B1CCB'
  }else if (buildStatus == 'ĐANG BUILD NÈ') {
    color = 'BLUE'
    colorCode = '#0B1CCB'
  }else if (buildStatus == 'ĐANG DEPLOY NÈ') {
    color = 'BLUE'
    colorCode = '#0B1CCB'
  }else if (buildStatus == 'XONG RỒI ÔNG GIÁO ƠI') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  // Send notifications
  slackSend (color: colorCode, message: msg_details)
}
```
# how to use

```
pipeline {
    agent any 
    stages {
        stage('Test') { 
            steps {
                // your step
               notifyBuild('CHẠY RỒI ÔNG GIÁO ƠI')
        	   notifyBuild('ĐANG TEST NÈ')
            }
        }
        stage('Build') { 
            steps {
                // your step
               notifyBuild('ĐANG BUILD NÈ')
            }
        }
        stage('Deploy') { 
            steps {
                // your step
               notifyBuild('ĐANG DEPLOY NÈ')
            }
        }
    }
    // aler slack
  post {
    success {
      echo "XONG RỒI ÔNG GIÁO ƠI"
      notifyBuild('XONG RỒI ÔNG GIÁO ƠI')
    }
    failure {
      echo "TOANG RỒI ÔNG GIÁO ƠI"
      notifyBuild('TOANG RỒI ÔNG GIÁO ƠI')
    }
    aborted{
      echo "CÓ THẰNG NÀO DỪNG KÌA"
      notifyBuild('CÓ THẰNG NÀO DỪNG KÌA')

    }
  }
}

