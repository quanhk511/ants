# script create user docker
```
#!/usr/bin/env bash
help() {
  cat <<EOF
  Arguments:
  +\$1 given username
  Usage example:
  $ ./add-docker-user.sh jenkins
EOF
}

# init vars
USR=$1
if [[ -z $USR ]]; then
  help
  exit 1
fi

# generate SSH key pairs
# REF. https://stackoverflow.com/a/43235320/1235074
ssh-keygen -q -N '' -m PEM -t rsa -f "$HOME/.ssh/id_rsa_$USR" <<< ""$'\n'"y" 2>&1 >/dev/null

# create new user
useradd -m -d /home/$USR -s /bin/bash $USR
usermod -aG docker $USR
mkdir /home/$USR/.ssh
touch /home/$USR/.ssh/authorized_keys
cat "$HOME/.ssh/id_rsa_$USR.pub" > /home/$USR/.ssh/authorized_keys
ssh -i $HOME/.ssh/id_rsa_$USR $USR@localhost "docker --version && echo '>>> DONE. New user added'" 
```
# script start jenkin-server   
```
#!/usr/bin/env bash
mkdir -p /home/jenkins/data
cd /home/jenkins
docker run -v /var/run/docker.sock:/var/run/docker.sock -v $(which docker):$(which docker) -v `pwd`/data:/var/jenkins_home  -p 8080:8080  --user 1000:999 --name jenkins-server -d jenkins/jenkins:lts
```
# docker-compose
```
version: '3.3'
services:
    jenkins:
        container_name: jenkins-server 
        hostname      : jenkin-c
        image         : jenkins/jenkins:lts
        user          : jenkins
        volumes:
            - '/var/run/docker.sock:/var/run/docker.sock'
            - '/usr/bin/docker:/usr/bin/docker'
            - '/data:/var/jenkins_home'
        ports:
            - '8080:8080'
```
# plugins after install jenkin-server
- Docker plugin    
- Gitlab Hook Plugin       
- GitLab Plugin       


