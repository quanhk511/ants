# Jenkins Groovy scripting for live fetching of Docker images    
makesure you have Jenkins Active Choice Parameter plugin      
href: [Link](https://kublr.com/blog/advanced-jenkins-groovy-scripting-for-live-fetching-of-docker-images/)  
href: [Link video](https://www.youtube.com/watch?v=bYi4IXep2mk&t=385s)     
active choice parametter
```
name: env  
return[
'Production',
'Sandbox',
'Development'
]
```
active choice reactive parametter   
```
name: imagetag
Referenced parameters: env	
// Import the JsonSlurper class to parse Dockerhub API response
import groovy.json.JsonSlurper


String filter = "";

if (env.equals("Production")){
    filter = "latest";
} else if(env.equals("Sandbox")){
	filter = "sandbox";
} else if(env.equals("Development")){
	filter = "dev";
} else {
	return ["Select a server from dropdown"]
}

docker_image_tags_url = "https://registry.adx.vn/v2/form-be/tags/list"
try {
// Set requirements for the HTTP GET request, you can add Content-Type headers and so on...
    def http_client = new URL(docker_image_tags_url).openConnection() as HttpURLConnection
    http_client.setRequestMethod('GET')

    String userCredentials = "registry:Sys@123465";
    String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));

    http_client.setRequestProperty ("Authorization", basicAuth);
    
    // Run the HTTP request
    http_client.connect()
    // Prepare a variable where we save parsed JSON as a HashMap, it's good for our use case, as we just need the 'name' of each tag.
    def dockerhub_response = [:]    
    // Check if we got HTTP 200, otherwise exit
    if (http_client.responseCode == 200) {
        dockerhub_response = new JsonSlurper().parseText(http_client.inputStream.getText('UTF-8'))
    } else {
        println("HTTP response error")
        System.exit(0)
    }
    // Prepare a List to collect the tag names into
    //def image_tag_list = []
    def dev_tag_list = []
    def image_tag_list = dockerhub_response.tags
    image_tag_list.each { tag_metada ->
    	if (tag_metada.contains(filter)) {
    		dev_tag_list.add(tag_metada)
    	}
    }
    return dev_tag_list.sort()
    // The returned value MUST be a Groovy type of List or a related type (inherited from List)
    // It is necessary for the Active Choice plugin to display results in a combo-box
} catch (Exception e) {
        // handle exceptions like timeout, connection errors, etc.
    return println(e)
}
```

