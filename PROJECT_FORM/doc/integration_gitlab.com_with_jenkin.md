# how to integration gitlab.com with jenkin-server
href: [link](https://blog.cloud365.vn/Tich-hop-Jenkins-vs-Gitlab-phan1/)
makesure you have gitlab plugins
1. add Personal Access Tokens 
- Log in to GitLab.   
- In the upper-right corner, click your avatar and select Settings.  
- On the User Settings menu, select Access Tokens.  
- Choose a name and optional expiry date for the token.  
- Choose the desired scopes.  
- Click the Create personal access token button.    
- Save the personal access token somewhere safe. Once you leave or refresh the page, you won’t be able to access it again        
Scopes:
- api   
- read_repository    

2. add Gitlab.com
- Manage jenkins > gitlab    
- unchecked Enable authentication for '/project' end-point   
- Connection name: xxxxx      
- Gitlab host URL: https://gitlab.com        
- Credentials: add token in step above  
- Test connection
