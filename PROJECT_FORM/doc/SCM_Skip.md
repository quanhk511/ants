# how to ignore push request in jenkin-server    
makesure you have SCM Skip plugins   
jenkinsfile   
```
    // ignore commit
    stage("Checkout") {
      agent { node {label 'master'}}
      steps {
        scmSkip(deleteBuild: true, skipPattern:'.*\\[ci skip\\].*')
      }
    } 
```
# how to use  

add [ci skip] to your commit   
ex:
```
git commit -m "xxxx [ci skip]"
```
