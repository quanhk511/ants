#!/bin/bash
wait4ready() {
    local c=$1   # c  aka container name to read log
    local kw=$2  # kw aka keyword to search in log

    echo "Ensure container $c exists ..."
    docker ps --format '{{.Names}} {{.Ports}}' | grep "$c "
        [ $? != 0 ] && (echo "Container $c not found"; kill $$)

    printf "Wait till container $c ready ..."
    while true; do
        docker logs -t "$c"  | grep -n0 "$kw"  --color=always
        [ $? == 0 ] && break
        printf '.' ; sleep 1s
    done

    echo "Your container $c is ready"
}
 
