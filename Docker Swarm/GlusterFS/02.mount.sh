#!/bin/bash
# auto mount after reboot server
# run script on each node!!!

docstring='
VOL_NAME=gfs MOUNT_PATH=/mnt \
 ./02.mount.sh

' 
[ -z "$VOL_NAME"   ] && (echo 'Envvar $VOL_NAME is required'; kill $$)
[ -z "$MOUNT_PATH" ] && (echo 'Envvar $MOUNT_PATH is required'; kill $$)

echo 'localhost:/$VOL_NAME /$MOUNT_PATH glusterfs defaults,_netdev,backupvolfile-server=localhost 0 0' >> /etc/fstab
mount.glusterfs localhost:/$VOL_NAME /$MOUNT_PATH
echo 'done'
