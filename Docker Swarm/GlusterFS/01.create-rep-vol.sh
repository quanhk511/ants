#!/bin/bash
docstring='
VOL_NAME=gfs REPLICA=3 \ 
PATH_BRICK_1=gfs01:/gluster/bricks/1/brick \
PATH_BRICK_2=gfs02:/gluster/bricks/2/brick \
PATH_BRICK_3=gfs03:/gluster/bricks/2/brick \
 ./01.create-rep-vol.sh

' 
echo ' please check your brick on each nodes '
echo; sleep 5
gluster pool list
sleep 5

[ -z "$VOL_NAME"   	 ] && (echo 'Envvar $VOL_NAME is required'; kill $$)
[ -z "$REPLICA"      ] && (echo 'Envvar $REPLICA is required'; kill $$)
[ -z "$PATH_BRICK_1" ] && (echo 'Envvar $PATH_BRICK_1 is required'; kill $$)
[ -z "$PATH_BRICK_2" ] && (echo 'Envvar $PATH_BRICK_2 is required'; kill $$)
[ -z "$PATH_BRICK_3" ] && (echo 'Envvar $PATH_BRICK_3 is required'; kill $$)

# create volume replica 3 brick
gluster volume create $VOL_NAME \
  				replica $REPLICA \
  					$PATH_BRICK_1 \
 					 $PATH_BRICK_2 \
  					$PATH_BRICK_3
# check volume
gluster volume list
# start volume
gluster volume start $VOL_NAME
# check status
gluster volume status $VOL_NAME

