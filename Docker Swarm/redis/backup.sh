#!/bin/bash
# ref.https://hoctapit.com/backup-va-restore-data-redis/
ts=`date +%Y%m%d_%H%M%S`
backup_tar_fn="backup_$ts.tar.gz"
DH='volume-bind-mount' 
redis-cli config get dir
redis-cli save
cd $DH
tar -czvf $backup_tar_fn dump.rdb  && cp $backup_tar_fn $HOME/backup
