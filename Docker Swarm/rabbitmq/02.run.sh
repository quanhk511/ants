 #!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
set -a
source "$SH/.image-push.env"
source "$SH/.config.env"

	[ $? != 0 ] && (echo  "ERROR cannot load $AH/.env"; kill $$; exit 1)

	docker stack deploy -c $SH/docker-compose.yml rabbit
