#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
set -a
source "$SH/.image-push.env"
source "$SH/.config.env"

    DOCKER_BUILDKIT=1  docker build  --file "$SH/Dockerfile"  -t $IMAGE_TAG  $SH        
