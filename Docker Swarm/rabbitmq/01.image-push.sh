#!/bin/bash
DOCSTRING='
DOCKERHUB_PWD=XX DOCKERHUB_USER=XX ./01.image-push.sh
'
SH=$(cd `dirname $BASH_SOURCE` && pwd)

[ -z $DOCKERHUB_PWD ] && (echo 'Param DOCKERHUB_TOKEN is required'; kill $$; exit 1)
[ -z $DOCKERHUB_USER ] && (echo 'Param DOCKERHUB_USER is required'; kill $$; exit 1)

source "$SH/.image-push.env" # load IMAGE_NAME

        echo "$DOCKERHUB_TOKEN" | docker login registry.adx.vn --username $DOCKERHUB_USER  --password-stdin

            [ $? != 0 ] && (echo 'Login failed'; kill $$; exit 1)

                # push image ref. https://stackoverflow.com/a/45554728/248616
                docker push    $IMAGE_TAG
  