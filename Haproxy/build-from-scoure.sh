#!/bin/bash
docstring='
OS=ubuntu ./build-from-scoure.sh
'
[ -z "$OS"    ] && (echo 'Envvar $OS is required'; kill $$)
# CentOS RHEL:
centos='yum -y install make gcc perl pcre-devel zlib-devel'

# Debian/Ubuntu:
ubuntu='apt install make gcc perl pcre-devel zlib-devel'

[[ $OS == "ubuntu" ]] && eval $ubuntu || eval $centos

echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
echo "net.ipv4.ip_nonlocal_bind = 1" >> /etc/sysctl.conf
sysctl -p
MD5_HA='6476e50486247dd9707870d0843989d3'
# download source ver 2.2
cd $HOME \
&& curl -o haproxy-2.2.8.tar.gz http://www.haproxy.org/download/2.2/src/haproxy-2.2.8.tar.gz \
&& echo "$MD5_HA haproxy-2.2.8.tar.gz" | sha1sum -c - \
&& tar xvzf haproxy-2.2.8.tar.gz && cd haproxy-2.2.8.tar.gz \
&& make TARGET=linux2628 USE_PCRE=1 USE_OPENSSL=1 USE_ZLIB=1 \
&& make install \
&& id -u haproxy &> /dev/null || useradd -s /usr/sbin/nologin -r haproxy \
&& cp haproxy /usr/sbin/ \
&& wget -qO - https://raw.githubusercontent.com/horms/haproxy/master/doc/configuration.txt | gzip -c > /usr/share/doc/haproxy/configuration.txt.gz \
&& wget https://gist.githubusercontent.com/tmidi/1699a358533ae876513e2887fec6fbe2/raw/6c07ce39adc56c731d2bbeb88b90d8bbc636f3ea/haproxy.service -O /lib/systemd/system/haproxy.service \
&& systemctl enablle haproxy.service \
&& systemctl start haproxy.service \
&& echo done
