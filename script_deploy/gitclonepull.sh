#!/bin/bash

SH=$(cd `dirname $BASH_SOURCE` && pwd)
DH=$(cd "$SH/.." && pwd)  # DH aka DEPLOY_HOME

set -e  # halt if error ON

export GIT_CLONE_HOME="/root"

gitclonepull() {
    docstring='
    ref. cutt.ly/nngit
    gitclonepull :todir :branch :repo
    '
    to_dir=$1
    branch=$2
      repo=$3
    sshkey=$4

    echo
    echo $repo
    echo $to_dir

    echo; eval $(ssh-agent -s) ; chmod 600 $sshkey; cat "$sshkey" | tr -d '\r' | ssh-add - 

    echo
    g="ssh -i $sshkey  -o PreferredAuthentications=publickey -o PasswordAuthentication=no"
    if [ ! -d $to_dir ]; then  # first time here --> git clone
        mkdir -p $to_dir
        (cd $to_dir && GIT_SSH_COMMAND=$g git clone --branch=$branch $repo .)
    elif [ -d $to_dir ]; then  # already gitcloned --> git pull
        (cd $to_dir && GIT_SSH_COMMAND=$g git reset -- . && git checkout $branch && git pull)
    fi

    # print log & branch
    (cd $to_dir && GIT_SSH_COMMAND=$g git log --oneline -n6)
    (cd $to_dir && GIT_SSH_COMMAND=$g git branch | grep '*')
}
        # git clone/pull 
          repo="git@gitlab.com:quanhk511/wp_demo.git"
        branch='devops'
         todir="$GIT_CLONE_HOME/wordpress/"
        sshkey="$SH/_sshkey/minhquan"
        gitclonepull $todir $branch $repo $sshkey
